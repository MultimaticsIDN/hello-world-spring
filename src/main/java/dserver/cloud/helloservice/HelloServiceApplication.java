package dserver.cloud.helloservice;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.SpringVersion;
import org.springframework.scheduling.annotation.EnableScheduling; 
  
@Configuration
@EnableAutoConfiguration 
@ComponentScan
@SpringBootApplication
@EnableScheduling 
public class HelloServiceApplication  {
	private static final Logger logger = LoggerFactory.getLogger(HelloServiceApplication.class);
	public static void main(String[] args) {
		SpringApplication springApplication=new SpringApplication(HelloServiceApplication.class);
		System.out.println("Spring Core Version:- " + SpringVersion.getVersion());
		SpringApplication.run(HelloServiceApplication.class, args);
		logger.debug("This is a debug message.");
        logger.info("This is an info message.");
        logger.warn("This is a warning message.");
        logger.error("This is an error message."); 
	}
	
	@Bean
	 public DataSource getDataSource() {
	    return DataSourceBuilder.create()
	      .driverClassName("com.mysql.cj.jdbc.Driver")
	      .url("jdbc:mysql://localhost:3306/myDb")
	      .username("user1")
	      .password("pass")
	      .build();	
	} 

}
